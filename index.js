/**
 * @format
 */

import { firebase } from '@react-native-firebase/messaging'
import { AppRegistry, NativeModules, Platform } from 'react-native'

import App from './App'
import { name as appName } from './app.json'
import { displayEventNotification } from './src/services/notifications/Notifications'

// fix for https://github.com/kmagiera/react-native-gesture-handler/issues/320
if (Platform.OS === 'android') {
  const { UIManager } = NativeModules
  if (UIManager) {
    // Add gesture specific events to genericDirectEventTypes object exported from UIManager native module.
    // Once new event types are registered with react it is possible to dispatch these events to all kind of native views.
    UIManager.genericDirectEventTypes = {
      ...UIManager.genericDirectEventTypes,
      onGestureHandlerEvent: { registrationName: 'onGestureHandlerEvent' },
      onGestureHandlerStateChange: {
        registrationName: 'onGestureHandlerStateChange'
      }
    }
  }
}

AppRegistry.registerComponent(appName, () => App)

if (Platform.OS === 'android') {
  // Listen for FCM push notifications when app is closed
  firebase.messaging().setBackgroundMessageHandler(async remoteMessage => {
    const debug = require('debug')('ditto:index:BackgroundMessageHandler')
    debug('Received FCM Message while in Background:', remoteMessage)

    const data = remoteMessage.data
    if (data.event_id != null && data.room_id != null) {
      displayEventNotification(data.event_id, data.room_id)
    } else {
      debug(`Problem with eventId (${data.event_id}) or roomId(${data.room_id})`)
    }
  })
}
