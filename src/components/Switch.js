import React, { Component } from 'react'
import { LayoutAnimation } from 'react-native'
import styled from 'styled-components/native'

export default class Switch extends Component {
  state = {
    switchState: true
  };

  handleToggleLeft = () => {
    this.props.onToggle && this.props.onToggle(true)
    LayoutAnimation.configureNext(LayoutAnimation.Presets.easeInEaseOut)
    this.setState({ switchState: true })
  };

  handleToggleRight = () => {
    this.props.onToggle && this.props.onToggle(false)
    LayoutAnimation.configureNext(LayoutAnimation.Presets.easeInEaseOut)
    this.setState({ switchState: false })
  };

  render () {
    return (
      <SwitchWrapper style={this.props.style}>
        <LeftLabel onPress={this.handleToggleLeft}>
          {this.props.leftLabel || 'Left'}
        </LeftLabel>
        <RightLabel onPress={this.handleToggleRight}>
          {this.props.rightLabel || 'Right'}
        </RightLabel>
        <SwitchInner switchState={this.state.switchState} />
      </SwitchWrapper>
    )
  }
}

const SwitchWrapper = styled.View`
  position: relative;
  background-color: ${({ theme }) => theme.darkAccentColor};
  /* width: 200; */
  height: 45;
  border-radius: 40;
`

const SwitchInner = styled.View`
  position: absolute;
  border-radius: 40;
  top: 0;
  ${({ switchState }) => (switchState ? 'left: 0;' : 'right: 0;')}
  width: 50%;
  height: 45;
  background-color: ${({ theme }) => theme.primaryAccentColor};
`

const LeftLabel = styled.Text`
  /* background-color: pink; */
  width: 50%;
  height: 45;
  position: absolute;
  top: 0;
  z-index: 1;
  text-align: center;
  line-height: 45;

  font-weight: bold;
  font-size: 20;
  color: ${({ theme }) => theme.primaryTextColor};
`

const RightLabel = styled(LeftLabel)`
  right: 0;
`
