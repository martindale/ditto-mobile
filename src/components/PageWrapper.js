import React from 'react'
import styled from 'styled-components/native'

const SafeAreaPageWrapper = styled.SafeAreaView`
  flex: 1;
  background-color: ${({ theme }) => theme.backgroundColor};
  ${({ justifyCenter }) => (justifyCenter ? 'justify-content: center;' : '')}
  ${({ alignCenter }) => (alignCenter ? 'align-items: center;' : '')}
`

const ViewPageWrapper = styled.View`
  flex: 1;
  background-color: ${({ theme }) => theme.backgroundColor};
  ${({ justifyCenter }) => (justifyCenter ? 'justify-content: center;' : '')}
  ${({ alignCenter }) => (alignCenter ? 'align-items: center;' : '')}
`

const PageWrapper = ({ safeArea = true, children, ...props }) => {
  return safeArea ? (
    <SafeAreaPageWrapper {...props}>{children}</SafeAreaPageWrapper>
  ) : (
    <ViewPageWrapper {...props}>{children}</ViewPageWrapper>
  )
}

export default PageWrapper
