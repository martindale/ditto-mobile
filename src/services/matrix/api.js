import AsyncStorage from '@react-native-community/async-storage'
import axios from 'axios'
import { parse, stringify } from 'flatted/esm'
import { Platform } from 'react-native'
import { getDeviceName } from 'react-native-device-info'
import {
  MATRIX_PUSH_GATEWAY_APNS,
  MATRIX_PUSH_GATEWAY_FCM,
  MATRIX_PUSH_GATEWAY_URL
} from 'react-native-dotenv'

import { ROUTES } from '../../constants'
import { clientSelector } from '../../selectors'
import store from '../../store'
import { getItemFromCache, saveItemInCache } from '../../utilities/Caching'
import { formatMatrixEvent } from './actions'

const debug = require('debug')('ditto:api:matrix')

export const fetchAvatarForRoom = async roomId => {
  const homeserver = await retrieveHomeserverFromCache()
  const userData = await retrieveUserDataFromCache()
  const url = ROUTES.rooms.avatar(homeserver, userData.accessToken, roomId)
  try {
    debug('fetchAvatarForRoom() url: ', url)
    const response = await axios.get(url)
    debug('fetchAvatarForRoom() response: ', response)
  } catch (error) {
    debug('ERROR fetching avatar for room: ', error)
    return null
  }
}

export const getRoomTimeline = roomId => {
  if (!roomId) return []
  const matrixClient = clientSelector(store.getState())
  const room = matrixClient.getRoom(roomId)
  const messages = room.getLiveTimeline().getEvents()
  const ret = []
  messages.forEach(event => ret.push(formatMatrixEvent(event)))
  return ret
}

export const fetchPreviousMessages = async (roomId, lastToken = null) => {
  const homeserver = await retrieveHomeserverFromCache()
  const userData = await retrieveUserDataFromCache()
  const url = ROUTES.rooms.fetchPreviousMessages(
    homeserver,
    userData.accessToken,
    roomId,
    lastToken
  )
  try {
    // debug('fetchPreviousMessages() url: ', url);
    const response = await axios.get(url)
    // debug('fetchPreviousMessages() response: ', response);
    return response
  } catch (error) {
    debug('ERROR fetching previous messages: ', error)
    return null
  }
}

export const getAllRoomMembers = async roomId => {
  const homeserver = await retrieveHomeserverFromCache()
  const userData = await retrieveUserDataFromCache()
  const url = ROUTES.rooms.members(homeserver, userData.accessToken, roomId)
  const response = await axios.get(url)
  return response.data.chunk
}

export const getDisplayName = async userId => {
  const homeserver = await retrieveHomeserverFromCache()
  const url = ROUTES.users.displayName(homeserver, userId)
  debug('url thing', url)
  const response = await axios.get(url)
  debug('response', response)
  return response.data.displayname
}

export const searchUserDirectory = async searchTerm => {
  const homeserver = await retrieveHomeserverFromCache()
  const userData = await retrieveUserDataFromCache()
  const url = ROUTES.users.directory(homeserver, userData.accessToken)
  const body = {
    searchTerm
    // limit: 10
  }
  const response = await axios.post(url, body)
  debug('DIRECTORY SEARCH RESPONSE', response)
  return response.data.results
}

export const setPusherWithToken = async deviceToken => {
  const matrixClient = clientSelector(store.getState())
  if (matrixClient == null) {
    return null
  }

  const deviceName = await getDeviceName() // TODO: Get from matrix client?
  debug(`Registering ${deviceName} with token: ${deviceToken}`)

  let appId = null
  let pushkey = deviceToken
  if (Platform.OS === 'ios') {
    appId = MATRIX_PUSH_GATEWAY_APNS
    // Convert to base64 since that's what sygnal expects
    pushkey = Buffer.from(deviceToken, 'hex').toString('base64')
  } else if (Platform.OS === 'android') {
    appId = MATRIX_PUSH_GATEWAY_FCM
  }

  const pusher = {
    lang: 'en', // TODO:i18n: Get from device?
    kind: 'http',
    app_display_name: 'Ditto', // TODO: Get from app?
    device_display_name: deviceName,
    app_id: appId,
    pushkey: pushkey,
    data: {
      url: MATRIX_PUSH_GATEWAY_URL,
      format: 'event_id_only'
    },
    append: false
  }

  debug('Set Pusher: ', pusher)
  try {
    const response = await matrixClient.setPusher(pusher)
    debug('Pusher response: ', response)
    if (Object.keys(response).length > 0) {
      throw Error("Couldn't register pusher for your matrix homeserver")
    }
  } catch (e) {
    debug('Pusher error: ', e)
    throw Error("Couldn't register pusher for your matrix homeserver")
  }
}

export const getEventDetails = async (eventId, roomId) => {
  const details = {}
  const matrixClient = clientSelector(store.getState())

  const messageEvent = await matrixClient.fetchRoomEvent(roomId, eventId)
  debug('Matrix event:', messageEvent)

  if (!messageEvent.type === 'm.room.message') {
    throw Error(`Unhandled event type: ${messageEvent.type}`)
  }

  details.message = {
    id: eventId,
    sender: messageEvent.sender,
    timestamp: messageEvent.origin_server_ts
  }
  const content = messageEvent.content
  const msgtype = content.msgtype
  debug('Message event content:', content)
  if (msgtype === 'm.text' || msgtype === 'm.notice') {
    details.message.type = 'text'
    details.message.content = { text: content.body }
  } else if (msgtype === 'm.image') {
    details.message.type = 'image'
    details.message.content = {
      text: 'sent you an image',
      url: content.info.thumbnail_url,
      info: content.info.thumbnail_info
    }
  } else {
    throw Error(`Unhandled message type: ${msgtype}`)
  }

  const me = matrixClient.getUser(matrixClient.getUserId())
  const meAvatar = matrixClient.mxcUrlToHttp(me.avatarUrl, 50, 50, 'crop')
  details.me = {
    id: me.userId,
    name: 'Me', // Same name as in Chats
    avatar: meAvatar
  }

  if (me.userId === messageEvent.sender) {
    details.sender = details.me
  } else {
    const sender = matrixClient.getUser(messageEvent.sender)
    const senderAvatar = matrixClient.mxcUrlToHttp(
      sender.avatarUrl,
      50,
      50,
      'crop'
    )
    details.sender = {
      id: sender.userId,
      name: sender.displayName,
      avatar: senderAvatar
    }
  }

  const room = matrixClient.getRoom(roomId)
  details.room = {
    id: roomId,
    title: room.name,
    avatar: room.avatar_small,
    isDirect: room.isDirect
  }

  debug('Event details: ', details)
  return details
}

export const getAll = async requests => {
  const response = await axios.all(requests)
  return response
}

export const saveUserDataToCache = data => {
  return saveItemInCache('userData', data)
}

export const retrieveUserDataFromCache = async () => {
  return getItemFromCache('userData')
}

export const saveHomeserverToCache = homeserver => {
  return saveItemInCache('homeserver', homeserver)
}

export const retrieveHomeserverFromCache = async () => {
  return getItemFromCache('homeserver')
}

export const saveCurrentRoomToCache = room => {
  return AsyncStorage.setItem('currentRoom', stringify(room))
}

export const retrieveCurrentRoomFromCache = async () => {
  const value = await AsyncStorage.getItem('currentRoom')
  return value ? parse(value) : null
}

export const saveRoomListToCache = roomList => {
  return AsyncStorage.setItem('roomList', stringify(roomList))
}

export const retrieveRoomListFromCache = async () => {
  const value = await AsyncStorage.getItem('roomList')
  return parse(value)
}

export const saveMessageListToCache = (roomId, messageList) => {
  return AsyncStorage.setItem(`${roomId}messageList`, stringify(messageList))
}

export const retrieveMessageListFromCache = async roomId => {
  const value = await AsyncStorage.getItem(`${roomId}messageList`)
  return parse(value)
}

export const saveDisplayNameListToCache = nameList => {
  return AsyncStorage.setItem('nameList', stringify(nameList))
}

export const retrieveDisplayNameListFromCache = async () => {
  const value = await AsyncStorage.getItem('nameList')
  return value ? parse(value) : []
}
