const dittoPurple = '#7817B4'
const darkAccent = '#0f0c1d'
const veryLight = '#E9E2F2'

// DARK Theme
const theme = {
  dittoPurple,
  backgroundColor: '#000',
  chromeColor: darkAccent,
  primaryTextColor: veryLight,
  // secondaryTextColor: '#938C9B80',
  secondaryTextColor: '#E9E2F250',
  primaryAccentColor: '#E90F9F',
  secondaryAccentColor: '#0089FF',
  darkAccentColor: darkAccent,
  iconColor: veryLight,
  errorRed: '#B51C4B',
  chat: {
    otherBubbleBackground: darkAccent,
    myBubbleBackground: dittoPurple,
    composerBackground: darkAccent
  },
  placeholderTextColor: '#E9E2F260'
}
export default theme
