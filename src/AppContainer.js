import React, { Component } from 'react'
import { ThemeProvider } from 'styled-components'

import AppNavigator from './services/navigation/AppNavigator'
import NavigationService from './services/navigation/NavigationService'
import theme from './theme'

class AppContainer extends Component {
  render () {
    return (
      <ThemeProvider theme={theme}>
        <AppNavigator
          ref={navigatorRef => {
            NavigationService.setTopLevelNavigator(navigatorRef)
          }}
        />
      </ThemeProvider>
    )
  }
}
export default AppContainer
