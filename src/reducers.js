import { combineReducers } from 'redux'

import AppReducer from './services/app/reducer'
import MatrixReducer from './services/matrix/reducer'

export default combineReducers({
  matrix: MatrixReducer,
  app: AppReducer
})
