import AsyncStorage from '@react-native-community/async-storage'
import { stringify } from 'flatted'
import { Cache } from 'react-native-cache'

const debug = require('debug')('ditto:caching')

const cache = new Cache({
  namespace: 'ditto',
  policy: {
    maxEntries: 50000
  },
  backend: AsyncStorage
})

export const clearCache = () => {
  cache.clearAll(err => {
    debug('There was an error clearing the cache: ', err)
  })
}

export const logCache = () => {
  cache.getAll((err, entries) => {
    if (err) {
      debug('Could not get the cache entries: ', err)
    } else {
      debug('Cache entries: ', entries)
    }
  })
}

export const saveItemInCache = (key, value) => {
  debug('Save item to cache: ', value)
  cache.setItem(key, value, err => {
    if (err) {
      throw Error(`Could not save value to cache: ${stringify(value)}`)
    }
  })
}

export const getItemFromCache = key => {
  debug('get item from cache: ', key)
  return new Promise((resolve, reject) => {
    cache.getItem(key, (err, value) => {
      if (err) {
        const e = new Error(`No item with key ${key} found in cache.`)
        debug(e.message)
        reject(e)
      } else {
        // debug(`Value found for key ${key}: `, value);
        resolve(value)
      }
    })
  })
}
