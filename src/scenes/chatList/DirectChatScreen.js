import React from 'react'
import { connect } from 'react-redux'
import styled from 'styled-components/native'

import { newChatModalVisibleSelector } from '../../selectors'
import { updateNewChatModalVisible } from '../../services/app/actions'
import ChatList from './components/ChatList'

class DirectChatScreen extends React.Component {
  render () {
    return (
      <Wrapper>
        <ChatList />
      </Wrapper>
    )
  }
}
const mapState = state => ({
  newChatModalVisible: newChatModalVisibleSelector(state)
})
const mapDispatch = {
  updateNewChatModalVisible
}
export default connect(mapState, mapDispatch)(DirectChatScreen)

const Wrapper = styled.View`
  flex: 1;
  background-color: ${({ theme }) => theme.backgroundColor};
`
