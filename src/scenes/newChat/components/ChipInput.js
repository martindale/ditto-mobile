'use strict'

import React, { Component } from 'react'
import {
  StyleSheet,
  View
} from 'react-native'
import {
  Chip,
  Selectize as ChildEmailField
} from 'react-native-material-selectize'
import { connect } from 'react-redux'

import { SCREEN_HEIGHT, SCREEN_WIDTH } from '../../../constants'
import { clientSelector } from '../../../selectors'
import theme from '../../../theme'
import ChipInputListItem from './ChipInputListItem'

// const Placeholder = require('../../assets/images/placeholder.png');

class ChipInput extends Component {
  static defaultProps = {
    onChipClose: () => {},
    onSubmitEditing: () => {}
  };

  constructor (props) {
    super(props)
    this.state = {
      error: null
    }
  }

  focus = () => {
    this._childEmailField.focus()
  };

  blur = () => {
    this._childEmailField.blur()
  };

  getSelectedEmails = () => this._childEmailField.getSelectedItems().result;

  isErrored = () => {
    return !!this.state.error
  };

  validate = email => {
    const { onSubmitEditing } = this.props

    if (
      this.getSelectedEmails().length >= 10 &&
      this._childEmailField.state.text
    ) {
      this.setState({ error: 'Sorry, you can enter a maximum of 10 emails' })
      onSubmitEditing(false)
      return false
    } else if (
      (email === '' && this.getSelectedEmails().length) ||
      /^@\w+([.-]?\w+)*:\w+([.-]?\w+)*(.\w{2,3})+$/.test(email.trim())
    ) {
      // http://www.w3resource.com/javascript/form/email-validation.php
      this.setState({ error: null })
      this.props.updateSelectedUsers([...this.getSelectedEmails(), email])
      onSubmitEditing(true)
    } else {
      this.setState({ error: 'Please enter a valid user' })
      onSubmitEditing(false)
      return false
    }
  };

  onSubmitEditing = email => {
    return this.validate(email)
  };

  onChipClose = (onClose, item) => {
    const { onChipClose } = this.props
    const { error } = this.state

    onChipClose(!error && this.getSelectedEmails().length > 1)
    onClose()

    const newList = this.getSelectedEmails()
    this.props.updateSelectedUsers(
      newList.filter(user => user !== item.user_id)
    )
  };

  render () {
    const { items } = this.props
    const { error } = this.state

    return (
      <View>
        <ChildEmailField
          ref={c => (this._childEmailField = c)}
          chipStyle={styles.chip}
          chipIconStyle={styles.chipIcon}
          error={error}
          itemId='user_id'
          items={items}
          label={null}
          listStyle={styles.list}
          tintColor='#028fb0'
          trimOnSubmit={false}
          showItems='always'
          textInputProps={{
            onSubmitEditing: this.onSubmitEditing,
            onBlur: () => this._childEmailField.submit(),
            placeholder: 'Who do you want to chat with?',
            placeholderTextColor: 'rgba(255,255,255,.3)',
            keyboardType: 'default',
            style: inputStyle,
            autoFocus: true,
            onChangeText: this.props.onChangeText
          }}
          containerStyle={containerStyle}
          renderRow={(id, onPress, item) => {
            const avatarUrl = this.props.client.mxcUrlToHttp(
              item.avatar_url,
              80,
              80,
              'crop'
            )
            return (
              <ChipInputListItem
                id={id}
                onPress={onPress}
                item={item}
                avatarUrl={avatarUrl}
              />
            )
          }}
          renderChip={(id, onClose, item, style, iconStyle) => (
            <Chip
              key={id}
              iconStyle={iconStyle}
              onClose={() => this.onChipClose(onClose, item)}
              text={id}
              textStyle={chipTextStyle}
              style={style}
            />
          )}
        />
      </View>
    )
  }
}

const mapState = state => ({
  client: clientSelector(state)
})
export default connect(mapState)(ChipInput)

const containerStyle = {
  width: SCREEN_WIDTH * 0.84,
  paddingTop: 0,
  paddingBottom: 0,
  marginLeft: 12
}

const inputStyle = {
  color: theme.primaryTextColor,
  height: 36
}

const chipTextStyle = {
  color: theme.primaryTextColor
}

const styles = StyleSheet.create({
  chip: {
    paddingRight: 2,
    backgroundColor: theme.chromeColor
  },
  chipIcon: {
    height: 24,
    width: 24,
    backgroundColor: theme.primaryAccentColor
  },
  list: {
    height: SCREEN_HEIGHT * 2
  }
})
