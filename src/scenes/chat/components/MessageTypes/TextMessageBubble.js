import React, { Component } from 'react'
import Html from 'react-native-render-html'
import { connect } from 'react-redux'
import striptags from 'striptags'
import styled from 'styled-components/native'

import { COLORS } from '../../../../constants'
import { userDataSelector } from '../../../../selectors'
import theme from '../../../../theme'
import { getNameColor, isEmoji } from '../../../../utilities/Misc'

const debug = require('debug')('ditto:component:TextMessageBubble')

class TextMessageBubble extends Component {
  onLinkPress = url => {
    debug('url', url)
  };

  render () {
    const { userData, message, prevMessage, nextMessage } = this.props

    const sender = message.sender
    const prevSenderSame = prevMessage && prevMessage.sender === sender
    const nextSenderSame = nextMessage && nextMessage.sender === sender

    const props = {
      prevSenderSame,
      nextSenderSame
    }

    const displayText = message.content.formatted_body || message.content.body

    const senderName =
      this.props.displayNames[message.sender] || message.sender

    const reactions = []
    if (
      message.unsigned &&
      message.unsigned['m.relations'] &&
      message.unsigned['m.relations']['m.annotation']?.chunk?.length > 0
    ) {
      message.unsigned['m.relations']['m.annotation'].chunk.forEach(r => {
        reactions.push(r.key)
      })
      debug('reactions', reactions)
    }

    // debug('display text', displayText);

    if (userData.userId === sender) {
      return (
        <>
          {isEmoji(displayText) ? (
            <Emoji isUser {...props}>
              {striptags(displayText)}
            </Emoji>
          ) : (
            <MyBubble {...props}>
              <Html html={displayText} {...htmlProps} />
            </MyBubble>
          )}

          {!prevSenderSame && (
            <SenderText isUser color={getNameColor(message.sender)}>
              {senderName}
            </SenderText>
          )}
        </>
      )
    } else {
      return (
        <>
          {isEmoji(displayText) ? (
            <Emoji {...props}>{striptags(displayText)}</Emoji>
          ) : (
            <OtherBubble {...props}>
              <Html html={displayText} {...htmlProps} />
            </OtherBubble>
          )}

          {!prevSenderSame && (
            <SenderText color={getNameColor(message.sender)}>
              {senderName}
            </SenderText>
          )}
        </>
      )
    }
  }
}
const mapStateToProps = state => ({
  userData: userDataSelector(state)
})
export default connect(mapStateToProps, null)(TextMessageBubble)

const baseFontStyle = {
  color: theme.primaryTextColor,
  fontSize: 16,
  letterSpacing: 0.3,
  fontWeight: '400'
}

const tagsStyles = {
  blockquote: {
    borderLeftColor: COLORS.red,
    borderLeftWidth: 3,
    paddingLeft: 10,
    marginVertical: 10,
    opacity: 0.8
  },
  p: {}
}

const htmlProps = {
  baseFontStyle,
  tagsStyles
}

// const Reactions = styled.Text`
//   z-index: 1;
//   font-size: 20;
//   align-self: flex-end;
// `;

const Emoji = styled.Text`
  font-size: 45;
  margin-left: 18;
  margin-right: 8;
  margin-top: 4;
  margin-bottom: 4;
  align-self: ${({ isUser }) => (isUser ? 'flex-end' : 'flex-start')};
`

const Bubble = styled.View`
  padding-left: 14;
  padding-right: 14;
  padding-top: 8;
  padding-bottom: 8;

  margin-top: ${({ prevSenderSame }) => (prevSenderSame ? 2 : 2)};
  margin-bottom: ${({ nextSenderSame }) => (nextSenderSame ? 1 : 4)};
  margin-left: 8;
  margin-right: 8;

  max-width: 300;

  border-radius: 18;
`

const sharpBorderRadius = 5

const OtherBubble = styled(Bubble)`
  align-self: flex-start;
  background-color: ${({ theme }) => theme.chat.otherBubbleBackground};

  ${({ prevSenderSame }) =>
    prevSenderSame ? `border-top-left-radius: ${sharpBorderRadius};` : ''}
  ${({ nextSenderSame }) =>
    nextSenderSame ? `border-bottom-left-radius: ${sharpBorderRadius};` : ''}
`

const MyBubble = styled(Bubble)`
  background-color: blue;
  align-self: flex-end;
  background-color: ${({ theme }) => theme.chat.myBubbleBackground};

  ${({ prevSenderSame }) =>
    prevSenderSame ? `border-top-right-radius: ${sharpBorderRadius};` : ''}
  ${({ nextSenderSame }) =>
    nextSenderSame ? `border-bottom-right-radius: ${sharpBorderRadius};` : ''}
`

const SenderText = styled.Text`
  color: ${({ color }) => (color || 'pink')};
  font-size: 14;
  font-weight: 400;
  margin-left: 22;
  margin-right: 22;
  margin-top: 8;
  opacity: 0.8;
  ${({ isUser }) => (isUser ? 'text-align: right;' : '')};
`
