import React, { Component } from 'react'
import { AutoGrowingTextInput } from 'react-native-autogrow-textinput'
import Icon from 'react-native-vector-icons/MaterialCommunityIcons'
import styled from 'styled-components/native'

import ChevronsUp from '../../../assets/icons/icon-chevrons-up.svg'
import IconPlus from '../../../assets/icons/icon-plus.svg'
import { COLORS } from '../../../constants'
import theme from '../../../theme'
import { isIos } from '../../../utilities/Misc'

export default class Composer extends Component {
  state = {
    messageValue: ''
  };

  handleSend = () => {
    this.props.onSend(this.state.messageValue)
    this.setState({ messageValue: '' })
  };

  render () {
    const emptyMsg = this.state.messageValue.length === 0
    return (
      <Wrapper>
        <ActionButton style={{ padding: 10 }} onPress={this.props.onImagePick}>
          <IconPlus fill={theme.iconColor} width={18} height={18} />
        </ActionButton>
        <GrowingTextInput
          placeholder='Type a message...'
          placeholderTextColor='rgba(255,255,255,.3)'
          value={this.state.messageValue}
          onChangeText={messageValue => this.setState({ messageValue })}
        />
        <ActionButton
          disabled={emptyMsg}
          style={{ padding: isIos() ? 10 : 0 }}
          onPress={this.handleSend}
        >
          {isIos() ? (
            <ChevronsUp fill={theme.iconColor} width={16} height={16} />
          ) : (
            <Icon name='send' size={22} color='#fff' />
          )}
        </ActionButton>
      </Wrapper>
    )
  }
}

const Wrapper = styled.SafeAreaView`
  flex-direction: row;
  margin-top: 10;
  margin-bottom: 10;
`

const verticalPadding = isIos() ? 7 : 4
const GrowingTextInput = styled(AutoGrowingTextInput)`
  background-color: ${({ theme }) => theme.chat.composerBackground};
  flex: 1;
  border-radius: 20;
  padding-top: ${verticalPadding};
  padding-bottom: ${verticalPadding};
  padding-left: 14;
  padding-right: 14;
  color: ${COLORS.gray.one};
  font-size: 16;
  letter-spacing: 0.3;
  font-weight: 400;
  height: ${isIos() ? 32 : 35};
`

const actionButtonSize = isIos() ? 32 : 35
const ActionButton = styled.TouchableOpacity`
  background-color: ${({ theme }) => theme.primaryAccentColor};
  justify-content: center;
  align-items: center;
  border-radius: 40;
  margin-left: 8;
  margin-right: 8;
  width: ${actionButtonSize};
  height: ${actionButtonSize};
  align-self: flex-end;
  opacity: ${({ disabled }) => (disabled ? 0.5 : 1)};
`
