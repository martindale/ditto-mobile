import React, { Component } from 'react'
import { FlatList } from 'react-native'
import ImagePicker from 'react-native-image-picker'
import Icon from 'react-native-vector-icons/Entypo'
import { connect } from 'react-redux'
import styled from 'styled-components/native'

import { COLORS, SCREEN_HEIGHT } from '../../constants'
import {
  clientSelector,
  currentMessagesSelector,
  typingUsersSelector,
  userDataSelector
} from '../../selectors'
import {
  formatMatrixEvent,
  setCurrentMessages,
  setCurrentRoom
} from '../../services/matrix/actions'
import {
  fetchPreviousMessages,
  getAllRoomMembers,
  getRoomTimeline,
  retrieveDisplayNameListFromCache,
  saveDisplayNameListToCache
} from '../../services/matrix/api'
import { cancelNotifications } from '../../services/notifications/Notifications'
import theme from '../../theme'
import { isIos, showError, toImageBuffer } from '../../utilities/Misc'
import Composer from './components/Composer'
import TimelineRenderer from './components/TimelineRenderer'
import TypingIndicator from './components/TypingIndicator'

const showdown = require('showdown')
const mdConverter = new showdown.Converter()

const debug = require('debug')('ditto:screens:ChatScreen')

class ChatScreen extends Component {
  static navigationOptions = ({ navigation: nav }) => {
    const roomTitle = nav.getParam('roomTitle', '…')

    return {
      title: roomTitle,
      headerTitleStyle: {
        color: COLORS.gray.one
      },
      headerLeft: <BackButton onPress={() => nav.goBack()} />,
      headerRight: null
    }
  };

  chatScrollView = null;

  state = {
    homeserver: '',
    messages: [],
    lastToken: '',
    displayNameList: []
  };

  onSend = messageText => {
    const html = mdConverter.makeHtml(messageText)
    var content = {
      body: messageText,
      format: 'org.matrix.custom.html',
      formatted_body: html,
      msgtype: 'm.text'
    }

    this.sendTempMessage({
      content,
      type: 'm.room.message'
    })

    const roomId = this.props.navigation.getParam('roomId', null)
    this.props.client
      .sendEvent(roomId, 'm.room.message', content, '')
      .then(res => {
        debug('Message sent successfully', res)
      })
      .catch(err => {
        debug('Message could not be send: ', err)
      })
  };

  handleImagePick = () => {
    try {
      debug('onImagePick')
      ImagePicker.showImagePicker(async response => {
        const mxcUrl = await this.props.client.uploadContent(
          toImageBuffer(response.data),
          {
            onlyContentUri: true,
            name: response.fileName,
            type: response.type
          }
        )
        const roomId = this.props.navigation.getParam('roomId', null)

        var content = {
          url: mxcUrl,
          msgtype: 'm.image'
        }
        this.sendTempMessage({
          content,
          type: 'm.room.message'
        })

        await this.props.client.sendImageMessage(
          roomId,
          mxcUrl,
          {
            w: response.width,
            h: response.height,
            mimetype: response.type,
            size: response.fileSize
          },
          response.fileName
        )
        debug('onImagePick successful image send')
      })
    } catch (e) {
      debug('onImagePick error', e)
    }
  };

  sendTempMessage = message => {
    const _id = `${Math.random() * 10000}`
    const newMsg = {
      ...message,
      isTempMessage: true,
      _id,
      event_id: _id,
      sender: this.props.userData.userId
    }
    this.props.setCurrentMessages([newMsg, ...this.props.messages])
  };

  handleEndReached = async () => {
    try {
      const roomId = this.props.navigation.getParam('roomId', null)
      const response = await fetchPreviousMessages(
        roomId,
        this.state.lastToken
      )
      // debug('response', response);
      this.setState({ lastToken: response.data.end })
      const prevMessages = response.data.chunk.map(msg =>
        formatMatrixEvent(msg)
      )
      // debug('prevMessages', prevMessages);
      this.props.setCurrentMessages([...this.props.messages, ...prevMessages])
      this.chatScrollView.scrollToEnd()
    } catch (error) {
      debug("Error fetching previous messages, but it's probs fine")
    }
  };

  updateMemberNamesInCache = async members => {
    let nameList = await retrieveDisplayNameListFromCache()
    // debug('namelist', nameList);
    if (!nameList) nameList = []
    members.forEach(member => {
      if (!nameList[member.user_id]) {
        nameList[member.user_id] = member.content.displayname
      }
    })
    saveDisplayNameListToCache(nameList)
    this.setState({ displayNameList: nameList })
  };

  fetchRoom = async (roomId) => {
    if (roomId == null) {
      debug('RoomId is not set, going back to chatlist…')
      this.props.navigation.goBack()
      showError('An error occurred', "Couldn't open chat list")
    }
    this.props.setCurrentRoom(roomId)
    debug('roomId', roomId)
    const room = this.props.client.getRoom(roomId)
    this.props.navigation.setParams({ roomTitle: room.name })

    cancelNotifications(roomId)

    const members = await getAllRoomMembers(roomId)
    this.updateMemberNamesInCache(members)

    const messages = getRoomTimeline(roomId)
    debug('messages', messages)
    this.props.setCurrentMessages(messages.reverse())

    const response = await fetchPreviousMessages(roomId)
    this.setState({ lastToken: response.data.end }, this.handleEndReached)
  }

  //* *******************************************************************************
  // Lifecycle
  //* *******************************************************************************

  componentDidMount = async () => {
    const roomId = this.props.navigation.getParam('roomId', null)
    await this.fetchRoom(roomId)
  };

  componentDidUpdate = async (prevProps) => {
    const roomId = this.props.navigation.getParam('roomId', null)
    const prevRoomId = prevProps.navigation.getParam('roomId', null)
    if (roomId !== prevRoomId) {
      await this.fetchRoom(roomId)
    }
  }

  componentWillUnmount = () => {
    this.props.setCurrentMessages([])
    this.props.setCurrentRoom(null)
  };

  render () {
    const { messages, typingUsers } = this.props
    return (
      <Wrapper>
        <FlatList
          keyboardDismissMode='interactive'
          inverted
          data={messages}
          renderItem={({ item, index }) => (
            <TimelineRenderer
              message={item}
              displayNames={this.state.displayNameList}
              prevMessage={messages[index + 1]}
              nextMessage={messages[index - 1]}
            />
          )}
          onEndReached={this.handleEndReached}
          onEndReachedThreshold={0.5}
          keyExtractor={item => item.event_id}
          ListHeaderComponent={() => (
            <TypingIndicator
              typingUsers={typingUsers}
              lastMessage={messages[0]}
              roomId={this.props.navigation.getParam('roomId', null)}
            />
          )}
        />
        <Composer
          onSend={msg => this.onSend(msg)}
          onImagePick={this.handleImagePick}
        />
      </Wrapper>
    )
  }
}
const mapStateToProps = state => {
  return {
    userData: userDataSelector(state),
    client: clientSelector(state),
    messages: currentMessagesSelector(state),
    typingUsers: typingUsersSelector(state)
  }
}
const mapDispatchToProps = {
  setCurrentMessages,
  setCurrentRoom
}
export default connect(mapStateToProps, mapDispatchToProps)(ChatScreen)

const BackTouchable = styled.TouchableOpacity``
const BackButton = ({ onPress }) => (
  <BackTouchable onPress={onPress}>
    <Icon
      name='chevron-left'
      size={35}
      color={theme.iconColor}
      style={{ marginLeft: 4 }}
    />
  </BackTouchable>
)

const IosWrapper = styled.KeyboardAvoidingView`
  flex: 1;
  background-color: ${({ theme }) => theme.backgroundColor};
`

const AndroidWrapper = styled.View`
  flex: 1;
  background-color: ${({ theme }) => theme.backgroundColor};
`

const Wrapper = ({ children }) => {
  return isIos() ? (
    <IosWrapper
      behavior='padding'
      keyboardVerticalOffset={SCREEN_HEIGHT * 0.11}
    >
      {children}
    </IosWrapper>
  ) : (
    <AndroidWrapper>{children}</AndroidWrapper>
  )
}
